import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import json
import sys
import xlrd
from xml.dom import minidom
from progressbar import ProgressBar
from time import sleep
import time

projectname ={}
vpoolname ={}
varrayname ={}
rowdata ={}
viprvarray ={}
viprvpool ={}
viprproject ={}

checkflag=1
count=0;

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
user='root'
password='Dangerous123#'
srv="10.64.31.60:4443"
headerVal = {"Host":"10.64.31.60:4443",
                  "X-SDS-AUTH-TOKEN":"BAAcZEdYbTRYNmJCZlk1RnpTM2RxQlAzRFpTN2RzPQMAVAQADTE0NjU5MDQ3 NTAyNTACAAEABQA9dXJuOnN0b3JhZ2VvczpUb2tlbjo5OWJlOGQ4OC0xZjAyLTQ1MzQtYTc0MC03ZTdm NjExOTBjNzE6dmRjMQIAAtAP",
                  "Content-Type": "application/xml",
            }


def create_welcome_screen ():
    print "-------------------------------------------------------------------------------------"
    for i in range(1,3):
        print ""
    print "\t\t\t Welcome To Vipr Automated Voulme Creation\t\t\t"
    for i in range(1,3):
        print ""
    print "-------------------------------------------------------------------------------------"

    
def get_project_list():
    print "Reading Projects from VIPR .. Please Wait ..."
    url="https://"+srv+"/projects/bulk"
    
    try:
        r=requests.get(url,headers=headerVal,verify=False)
        #print r.status_code
        xmldoc = minidom.parseString(r.text)
        itemlist = xmldoc.getElementsByTagName('id')
        #print len(itemlist)
        for item in itemlist :
                 urn = (item.firstChild.nodeValue)
                 name = get_project_name(item.firstChild.nodeValue)
                 viprproject[name] = urn;
                 
                 
    except requests.exceptions.RequestException as e:
        # print e
        print "failed"
        sys.exit(1);
        
    #if r.status_code==200:
        #print "Success"
       

def get_project_name (str1):
    url ="https://"+srv+"/projects/"+str1
    try:
        r=requests.get(url,headers=headerVal,verify=False)
        #print r.status_code
        xmldoc = minidom.parseString(r.text);
        itemlist = xmldoc.getElementsByTagName('name')
        for item in itemlist :
            return(item.firstChild.nodeValue)
        
    except requests.exceptions.RequestException as e:
        # print e
        print "failed"
        sys.exit(1);
        
    #if r.status_code==200:
        #print "Success"
        

def get_virtual_array_list ():
    print "Reading Virtual Arrays from VIPR .. Please Wait ..."
    url ="https://"+srv+"/vdc/varrays/bulk"
    try:
        r=requests.get(url,headers=headerVal,verify=False)
        #print r.status_code
        xmldoc = minidom.parseString(r.text)
        itemlist = xmldoc.getElementsByTagName('id')
        #print len(itemlist)
        for item in itemlist :
                 urn=(item.firstChild.nodeValue)
                 name= get_virtual_array_name(item.firstChild.nodeValue)
                 viprvarray[name]=urn
                 
    except requests.exceptions.RequestException as e:
        # print e
        print "failed"
        sys.exit(1);
        
    #if r.status_code==200:
        #print "Success"




def get_virtual_array_name (str1):
    url ="https://"+srv+"/vdc/varrays/"+str1
    try:
        r=requests.get(url,headers=headerVal,verify=False)
        #print r.status_code
        xmldoc = minidom.parseString(r.text);
        itemlist = xmldoc.getElementsByTagName('name')
        for item in itemlist :
            return(item.firstChild.nodeValue)
        
    except requests.exceptions.RequestException as e:
        # print e
        print "failed"
        sys.exit(1);
        
    #if r.status_code==200:
        #print "Success"

def get_block_pool_list ():
    print "Reading Block Virtual Pool from VIPR .. Please Wait ..."
    url ="https://"+srv+"/block/vpools/bulk"
    try:
        r=requests.get(url,headers=headerVal,verify=False)
        #print r.status_code
        xmldoc = minidom.parseString(r.text)
        itemlist = xmldoc.getElementsByTagName('id')
        #print len(itemlist)
        for item in itemlist :
                 urn = (item.firstChild.nodeValue)
                 name=get_block_pool_name(item.firstChild.nodeValue)
                 viprvpool[name]=urn
                 
    except requests.exceptions.RequestException as e:
        # print e
        print "failed"
        sys.exit(1);
        
    #if r.status_code==200:
        #print "Success"


def get_block_pool_name (str1):
    url ="https://"+srv+"/block/vpools/"+str1
    try:
        r=requests.get(url,headers=headerVal,verify=False)
        #print r.status_code
        xmldoc = minidom.parseString(r.text);
        itemlist = xmldoc.getElementsByTagName('name')
        for item in itemlist :
            return(item.firstChild.nodeValue)
        
    except requests.exceptions.RequestException as e:
        # print e
        print "failed"
        sys.exit(1);
        
    #if r.status_code==200:
        #print "Success"
        

def read_design ():
    #pbar = ProgressBar()
    book = xlrd.open_workbook('C:\\EMC\\CAT\\test_wb.xlsx')                              
    max_nb_row =0
    for sheet in book.sheets():
        max_nb_row =max(max_nb_row, sheet.nrows)
    for row in range(1,max_nb_row):
            for sheet in book.sheets() :
                    if row < sheet.nrows:
                        currentrow = sheet.row(row)
                    #print "Volume Name : "+currentrow[6].value +"\t --> Size :"+ str(currentrow[10].value)+"\t --> Project Name :"+str(currentrow[22].value)+"\t virtual Array :"+str(currentrow[0].value)+"\t Virtual Pool :"+str(currentrow[7].value)
                    projectname[str(currentrow[22].value)] = 0
                    varrayname[str(currentrow[0].value)]=0
                    vpoolname[str(currentrow[5].value)] =0
                    #projectname;varrayname;vpoolname;size;lunname
                    rowdata[row] = str(currentrow[22].value)+";"+str(currentrow[0].value)+";"+str(currentrow[5].value)+";"+str(currentrow[10].value)+";"+str(currentrow[6].value)
    
def check_existance ():
    print "Verifing couple more things ..."
    checkflag=1;
    for k,v in projectname.items():
        if(k in viprproject):
            projectname[k] =1
    for k,v in varrayname.items():
        if(k in viprvarray):
            varrayname[k] =1
    for k,v in vpoolname.items():
        if(k in viprvpool):
            vpoolname[k] =1

    for k,v in projectname.items():
        if(v == 0):
            print "The project "+k+" Does not exist in VIPR please create it"
            checkflag=0

    for k,v in varrayname.items():
        if(v == 0):
            print "The Virtual Array "+k+" Does not exist in VIPR please create it"
            checkflag=0;

    for k,v in vpoolname.items():
        if(v == 0):
            print "The Virtual Pool "+k+" Does not exist in VIPR please create it"
            checkflag=0;

    if(checkflag==0):
        print "There are few elements which needs to created before the script. Please do them and run it again. Thank you"
        

            
def check_login ():
    url="https://"+srv+"/login"
    payload=""

    try:
        r=requests.get(url, auth=(user,password),verify=False)
    except requests.exceptions.RequestException as e:
       # print e
        print "failed"
        sys.exit(1);

    if r.status_code==200:
        print "Logged into Vipr controller at :"+srv
        sleep(5);

def frame_xml(projectname,varrayname,vpoolname,size,lunname):
    print "Creating "+lunname;
    xml_data = """<volume_create><name>"""+lunname+"""</name>
     <size>"""+size+""" GB</size>
     <count>1</count>
     <project>"""+projectname+"""</project>
     <varray>"""+varrayname+"""</varray>
     <vpool>"""+vpoolname+"""</vpool>
    </volume_create>"""
    print xml_data
    return xml_data


    
def create_volume ():

    if(checkflag==1):
        url="https://"+srv+"/block/volumes"
        for k , v in rowdata.items():
             data = v.split(';')
             xml_data = frame_xml(viprproject[data[0]],viprvarray[data[1]],viprvpool[data[2]],data[3],data[4])
             try:
                r=requests.post(url,data=xml_data,headers=headerVal,verify=False)
             except requests.exceptions.RequestException as e:
                # print e
                print "failed"
                sys.exit(1);
                
             if r.status_code==200 or r.status_code==202:
                print "Success -- Created volume "+data[4]
                sleep(10);
             else :
                 print 'x'
                 print r.status_code

create_welcome_screen()
check_login();
read_design()
get_project_list()
get_virtual_array_list()
get_block_pool_list()
check_existance()
create_volume();
