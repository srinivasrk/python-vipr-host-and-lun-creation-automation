import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import json
import sys
import xlrd
from xml.dom import minidom
from progressbar import ProgressBar
from time import sleep
import time
import getpass

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

srv="https://10.64.31.60:4443/";
user="root";
password="Dangerous123#";
authToken="BAAcWnNIeC9Nb3E2YXlQQ09qTlptYlN3QVNoaVlFPQMAVAQADTE0NzM5Mzk5 NTE5NjYCAAEABQA9dXJuOnN0b3JhZ2VvczpUb2tlbjo1NTA3NTBiYS0xYzg0LTRkNzktOTkxMS02MTlk ZjQwYjNhYzk6dmRjMQIAAtAP";
headerVal={}

def create_welcome_screen ():
    print "-------------------------------------------------------------------------------------"
    for i in range(1,3):
        print ""
    print "\t\t\t Welcome To Vipr Automation\t\t\t"
    for i in range(1,3):
        print ""
    print "-------------------------------------------------------------------------------------"


def create_exit_screen():
    print "-------------------------------------------------------------------------------------"
    for i in range(1,3):
        print ""
    print "\t\t\t Thanks for using VIPR Automated Script - EMC \t\t\t"
    for i in range(1,3):
        print ""
    print "-------------------------------------------------------------------------------------"




def check_login ():
    url="https://"+srv+"/login"
    payload=""

    try:
        r=requests.get(url, auth=(user,password),verify=False)
    except requests.exceptions.RequestException as e:
       # print e
        print "failed"
        sys.exit(1);

    if r.status_code==200:
        print "-------------------------------------------------------------------------------------"
        for i in range(1,3):
            print ""
        print "Logged into Vipr controller at : "+srv
        for i in range(1,3):
            print ""
        print "-------------------------------------------------------------------------------------"
        sleep(5);


def read_excel_data():
    path = raw_input("Enter the path of the excel file to be used for VIPR automation : ");
    book = xlrd.open_workbook(path)                              
    sheet = book.sheet_by_index(0)

    for row_index in range(1,sheet.nrows):
        if(len(str(sheet.cell_value(row_index,0))) <= 0):
            continue;
        else:
            varray_name = sheet.cell_value(row_index,0);
            vpool_name = sheet.cell_value(row_index,1);
            lun_name = sheet.cell_value(row_index,2);
            size = sheet.cell_value(row_index,3);
            project_name = sheet.cell_value(row_index,4);

            varray_uid = get_varray_uri(varray_name);
            vpool_uid = get_vpool_uri(vpool_name);
            project_uid = get_project_uri(project_name);
            create_lun(varray_uid,vpool_uid,lun_name,str(size),project_uid);
            
    sheet = book.sheet_by_index(1)

    for row_index in range(1,sheet.nrows):
        if(len(str(sheet.cell_value(row_index,0))) <= 0):
            continue;
        else:
            host_name = sheet.cell_value(row_index,0);
            #print host_name;
            host_os = sheet.cell_value(row_index,1);
            #print host_os
            os_version = sheet.cell_value(row_index,2);
            #print str(os_version)
            host_lbl = sheet.cell_value(row_index,3);
            #print host_lbl
            host_username = sheet.cell_value(row_index,4);
           # print host_username
            host_pwd = sheet.cell_value(row_index,5);
            #print host_pwd
            port = sheet.cell_value(row_index,6);
            #print str(port)
            is_ssl = sheet.cell_value(row_index,7);
            #print is_ssl
            if(is_ssl=="No"):
                is_ssl="false"
            else:
                is_ssl="true"
                
            cluster = sheet.cell_value(row_index,8);
            #print cluster
            tenant_name = sheet.cell_value(row_index,9);
            #print tenant_name

            tenant_uri = get_tenant_uri(tenant_name);
            cluster_uri="N/A"
            if(cluster != "N/A"):
                cluster_uri = get_cluster_uri(cluster);

            #print host_name + " "+host_os+" "+str(os_version)+" "+host_lbl+" "+str(port)+" "+host_username+" "+host_pwd+" "+is_ssl+" "+cluster_uri+" ";
            #print str(tenant_uri);
            
            create_host(host_name,host_os,str(os_version),host_lbl,(str(port)).rstrip('.0'),host_username,host_pwd,is_ssl,cluster_uri,str(tenant_uri));


            
def get_tenant_uri(str1):
    url ="https://"+srv+"/tenants/search/?name="+str1
    try:
        r = requests.get(url,headers=headerVal,verify=False)
        xmldoc = minidom.parseString(r.text);
        itemlist = xmldoc.getElementsByTagName('id')
        for item in itemlist :
            return(item.firstChild.nodeValue)
    except requests.exceptions.RequestException as e:
            # print e
            print "failed to get Virtual Array Details Please check the Varray name again !!"
            sys.exit(1);   

def get_cluster_uri(str1):
    url ="https://"+srv+"/compute/clusters/search/?name="+str1
    try:
        r = requests.get(url,headers=headerVal,verify=False)
        xmldoc = minidom.parseString(r.text);
        itemlist = xmldoc.getElementsByTagName('id')
        for item in itemlist :
            return(item.firstChild.nodeValue)
    except requests.exceptions.RequestException as e:
            # print e
            print "failed to get Virtual Array Details Please check the Varray name again !!"
            sys.exit(1);
            
 
def get_varray_uri(str1):
    url ="https://"+srv+"/vdc/varrays/search/?name="+str1
    try:
        r = requests.get(url,headers=headerVal,verify=False)
        xmldoc = minidom.parseString(r.text);
        itemlist = xmldoc.getElementsByTagName('id')
        for item in itemlist :
            return(item.firstChild.nodeValue)
    except requests.exceptions.RequestException as e:
            # print e
            print "failed to get Virtual Array Details Please check the Varray name again !!"
            sys.exit(1);     
        
def get_vpool_uri(str1):
    url ="https://"+srv+"/block/vpools/search/?name="+str1
    try:
        r = requests.get(url,headers=headerVal,verify=False)
        xmldoc = minidom.parseString(r.text);
        itemlist = xmldoc.getElementsByTagName('id')
        for item in itemlist :
            return(item.firstChild.nodeValue)
            
    except requests.exceptions.RequestException as e:
            # print e
            print "failed to get Virtual Array Details Please check the Varray name again !!"
            sys.exit(1);  


def get_project_uri(str1):
    url ="https://"+srv+"/projects/search/?name="+str1
    try:
        r = requests.get(url,headers=headerVal,verify=False)
        xmldoc = minidom.parseString(r.text);
        itemlist = xmldoc.getElementsByTagName('id')
        for item in itemlist :
            return(item.firstChild.nodeValue)
            
    except requests.exceptions.RequestException as e:
            # print e
            print "failed to get Virtual Array Details Please check the Varray name again !!"
            sys.exit(1);  

def create_lun(varrayuri,vpooluri,lunname,size,projecturi):
    url="https://"+srv+"/block/volumes"
    xml_data=""" <volume_create><name>"""+lunname+"""</name>
     <size>"""+size+"""GB</size>
     <count>1</count>
     <project>"""+projecturi+"""</project>
     <varray>"""+varrayuri+"""</varray>
     <vpool>"""+vpooluri+"""</vpool>
    </volume_create>"""
   
    try:
        r=requests.post(url,data=xml_data,headers=headerVal,verify=False)
    except requests.exceptions.RequestException as e:
        print e
        print "failed to create volume : "+ lunname + " !!!"
        sys.exit(1);
    if r.status_code==200 or r.status_code==202:
        print "Success -- Created volume "+lunname
        sleep(1);
    else :
        print r.status_code
        print r.content


    
def create_host(host_name,os_name,os_ver,host_name_lbl,port,host_username,host_pwd,is_ssl,cluster,tenant_uri):
    url="https://"+srv+"/compute/hosts"

    if(cluster == "N/A"):
        xml_data ="""<host_create>
        <type>"""+ os_name+"""</type>
        <host_name>"""+host_name+"""</host_name>
        <os_version>"""+os_ver+"""</os_version>
        <name>"""+host_name_lbl+"""</name>
        <port_number>"""+port+"""</port_number>
        <user_name>"""+host_username+"""</user_name>
        <password>"""+host_pwd+"""</password>
        <use_ssl>"""+is_ssl+"""</use_ssl>
        <discoverable>true</discoverable>
        <tenant>"""+tenant_uri+"""</tenant>
        </host_create>"""
    else:
        xml_data ="""<host_create>
        <type>"""+ os_name+"""</type>
        <host_name>"""+host_name+"""</host_name>
        <os_version>"""+os_ver+"""</os_version>
        <name>"""+host_name_lbl+"""</name>
        <port_number>"""+port+"""</port_number>
        <user_name>"""+host_username+"""</user_name>
        <password>"""+host_pwd+"""</password>
        <cluster>"""+cluster+"""</cluster>
        <use_ssl>"""+is_ssl+"""</use_ssl>
        <discoverable>true</discoverable>
        <tenant>"""+str(tenant_uri)+"""</tenant>
        </host_create>"""
        
    

    print xml_data;
    
    print "Loading (sending API REQUEST)... Please wait ...";
    try:
        r=requests.post(url,data=xml_data,headers=headerVal,verify=False)
    except requests.exceptions.RequestException as e:
        print e
        print "failed to create host : "+ host_name+" !!"
        sys.exit(1);
              
    if r.status_code==200 or r.status_code==202:
            print "Success -- Created Host "+host_name+" !";
            sleep(2);
    else :
            print r.status_code
            print r.content



    
create_welcome_screen();
raw_input("Press Enter to continue..")

srv= raw_input("Enter the VIPR IP Address : ")
srv=srv+":4443";
user=raw_input("Enter the VIPR User Name : ")
print("Enter the VIPR Password : ")
password =  getpass.getpass()
authToken = raw_input("Enter the AUTH TOKEN GENERATED FROM CURL : ")
headerVal = {"Host":srv,
             "X-SDS-AUTH-TOKEN": authToken,
             "Content-Type":"application/xml",
             }
check_login();
#create_host();
read_excel_data();
create_exit_screen();
