import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import json
import sys
import xlrd
from xml.dom import minidom
from progressbar import ProgressBar
from time import sleep
import time
import getpass
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

#GLOBAL VARIABLES

srv="localhost";
user="root";
password="";
authToken="";
headerVal={}

#WELCOME SCREEN
def create_welcome_screen ():
    print "-------------------------------------------------------------------------------------"
    for i in range(1,3):
        print ""
    print "\t\t\t Welcome To Vipr Automated Voulme Creation\t\t\t"
    for i in range(1,3):
        print ""
    print "-------------------------------------------------------------------------------------"

#CHECK IF LOGGED IN
def check_login ():
    url="https://"+srv+"/login"
    payload=""

    try:
        r=requests.get(url, auth=(user,password),verify=False)
    except requests.exceptions.RequestException as e:
       # print e
        print "failed"
        sys.exit(1);

    if r.status_code==200:
        print "Logged into Vipr controller at :"+srv
        sleep(5);

#CREATE HOST
def create_host():
    url="https://"+srv+"/compute/hosts"
    host_name = raw_input("Enter host name to be discovered in VIPR (fqdn.Hostname.com): ");
    print host_name;
    os_name = raw_input("Enter the OS (operating system of the host) : ");
    os_ver = raw_input("Enter the OS Version (Ex. 1.0 , 2.0 etc ) : ");
    port = raw_input("Enter the port number(Default: 5985 (http) or 5986 (https) enter one) : ");
    host_username = raw_input("Enter the username of the Host : ");
    host_pwd = getpass.getpass();
    tenant_uri = raw_input("Enter tenant URI where the host is to be created :");
    xml_data = """<host_create>
    <type>"""+ os_name+"""</type>
    <host_name>"""+host_name+"""</host_name>
    <os_version>"""+os_ver+"""</os_version>
    <name>"""+host_name+"""</name>
    <port_number>"""+port+"""</port_number>
    <user_name>"""+host_username+"""</user_name>
    <password>"""+host_pwd+"""</password>
    <use_ssl>false</use_ssl>
    <discoverable>true</discoverable>
    <tenant>"""+tenant_uri+"""</tenant>
    </host_create>"""

    print xml_data;
    
    #print srv;
    try:
        r=requests.post(url,data=xml_data,headers=headerVal,verify=False)
    except requests.exceptions.RequestException as e:
        # print e
        print "failed"
        sys.exit(1);
              
    if r.status_code==200 or r.status_code==202:
            print "Success -- Created Host ";
            sleep(10);
    else :
            print 'x'
            print r.status_code


#GET LOGIN DETAILS    
create_welcome_screen();
raw_input("Press any key to continue..")

srv= raw_input("Enter the VIPR IP Address : ")
srv=srv+":4443";
user=raw_input("Enter the VIPR User Name : ")
print("Enter the Password : ")
password =  getpass.getpass()
authToken = raw_input("Enter the AUTH TOKEN GENERATED FROM CURL : ")
headerVal = {"Host":srv,
             "X-SDS-AUTH-TOKEN": authToken,
             "Content-Type":"application/xml",
             }
check_login();
create_host();
