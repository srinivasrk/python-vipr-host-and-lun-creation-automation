# Python Automation (Py2Exe)#



### Summary ###

ViPR controller is EMC storage provisioning product which works across EMC and 3rd party device. The drawback is creating device/volume is manual, if you had to create 500 volumes of different size and names then ViPR does not have a function to do so. which makes the task very tedious.

This python application takes an Excel as input which has all the details of Luns and Names of hosts to be added to ViPR and using API programming with Python, we send the requests to the ViPR server.

This has reduced almost 75% of time on creating 585 volumes in a production environment.

### Sample Working ###

*![Capture2.PNG](https://bitbucket.org/repo/5pqKE6/images/1841799749-Capture2.PNG)

* ![Capture3.PNG](https://bitbucket.org/repo/5pqKE6/images/279480305-Capture3.PNG)

* ![Capture4.PNG](https://bitbucket.org/repo/5pqKE6/images/1319538135-Capture4.PNG)